﻿# Вопросы на интервью для junior .net dev

1. 	Скомпилируется ли следующий код: 

```cs
class Program 
{ 
	private static int a; 
	
	static Program(int a) 
	{ 
		Program.a = a; 
	} 
	
	static void Main(string[] args) 
	{ 
	} 
}
```

- Да
- **Нет**

> Объяснение: Статический конструктор класса не должен содержать параметров.

2. Какие из следующих модификаторов допустимы к методам в C#?

- **public**
- final
- strictfp
- **abstract**
- **internal**
- const

3. Каким будет результат компиляции и выполнения следующего кода:

```cs
class Program
{
	class A
	{
		public virtual void Print() { Console.Write("A"); }
	}
	class B : A
	{
		public override void Print() { Console.Write("B"); }
	}
	class C : B
	{
		public new void Print()
		{
			base.Print();
			Console.Write("C");
		}
	}

	static void Main(string[] args)
	{
		A a = new A();
		A b = new B();
		A c = new C();
		a.Print();
		b.Print();
		c.Print();
	}
}
```

- **ABB**
- ABA
- ABBA
- ABC
- AABC

4. Если класс A должен отнаследоваться от класса B, какая из следующих конструкция должна использоваться?

- class B extends A
- class B : A
- class A extends B
- **class A : B**

5. Скомпилируется ли данный код?

```cs
interface I 
{ 
	void G(); 
} 

class A 
{ 
	public void G() { } 
} 

class B : A, I 
{ 
	public static void Main() { } 
}
```

- **Да**
- Нет

> Объяснение: Метод G класса A используется в классе B как реализация интерфейса

6. Каким будет результат компиляции и выполнения следующего кода?

```cs
class A
{
	public A() { Console.WriteLine("A"); }
}
class B: A
{
	public B() { Console.WriteLine("B"); }
}
public class Test
{
	public static void Main()
	{
		A a = new B();
	}
}
```

- **A B**
- B
- B A
- Произойдёт ошибка компиляции
- A

> Объяснение: Вначале вызывается конструктор класса предка, затем потомка.

7. Каким будет результат компиляции и выполнения следующего кода: 

```cs
class A
{
	public int i;
	public A(int i) { this.i = i; }
}
class Program
{
	static void Main()
	{
		A a = new A(1) {i = 2};
		Console.WriteLine(a.i);
	}
}
```

- 0
- 1
- **2**
- Произойдёт ошибка компиляции

8. Скомпилируется ли следующий код?

```cs
interface A
{
	void f();
}
interface B
{
	void f();
}
class C : A, B
{
	void A.f() {}
	void B.f() {}
	
	public static void Main() {}
}
```

- Нет
- **Да**

> Объяснение: Такой код позволит сделать разные реализации метода f для каждого из реализованных интерфейсов. Это явная реализация интерфейсов.


9. Каким будет результат компиляции и выполнения следующего кода?

```cs
sealed class A { }
class B: A { }
public class Test
{
	public static void Main()
	{
		B b = new B();
		A a = b;
		Console.WriteLine(a == b);
	}
}
```

- False
- True
- Произойдёт ошибка выполнения
- **Произойдёт ошибка компиляции**

> Объяснение: Нельзя наследоваться от класса, объявленного с модификатором sealed.

10. Каким будет результат компиляции и выполнения кода:

```cs
class Program
{
	static void Main()
	{
		try
		{
			string str = null;
			Console.WriteLine(str.Length);
		}
		catch (Exception e)
		{
			Console.WriteLine("str = " + str);
		}
	}
}
```

- Ничего не будет выведено
- **Произойдёт ошибка компиляции**
- 0
- str = null
- -1

> Объяснение: Переменная str объявлена в блоке try и не попадает в область видимости блока catch.

11. Каким будет результат компиляции и выполнения следующего кода

```cs
public class Test
{
	public static void Main()
	{
		Console.WriteLine(method(1));
	}
	static int method(int i)
	{
		if (i != 0) { return 1; }
		else if (i == 0) { return 0; }
	}
}
```

- 1
- **Произойдёт ошибка компиляции**
- 0

> Объяснение: Несмотря на то, что все варианты в методе "method" учтены логически, нужно чтобы был либо безусловный return в конце метода, либо во вложенном if ветка else.

12. Ключевое слово sealed можно применить к

- Локальным переменным
- **Методам**
- Полям
- Интерфейсам

> Объяснение: Модификатор sealed можно использовать для метода или свойства, которое переопределяет виртуальный метод или свойство в базовом классе. Это позволяет классам наследовать от вашего класса, запрещая им при этом переопределять определенные виртуальные методы или свойства.

13. В результате каких команд на консоль будет выведено "33"?

- **Console.WriteLine("3" + "3");**
- Console.WriteLine(1 + "2" + "3");
- Console.WriteLine("3" + 1 + 2);
- **Console.WriteLine(2 + 1 + "3");**
- Console.WriteLine("3" + 4 - 1);

14. Какой результат компиляции и выполнения следующей программы?

```cs
class Program
{
	static void Main()
	{
		Func f;
		f = () => 1;
		f += () => 2;
		f += () => 3;
		Console.WriteLine(f());
	}
}
```

- **3**
- 2
- 1
- Произойдёт ошибка компиляции

15. Могут ли структуры реализовывать интерфейсы?

- **Да**
- Нет

16. Какой результат компиляции и выполнения следующей программы

```cs
class Program
{
	static void Main()
	{
		if (new int[5] is Object) { Console.WriteLine("Object"); }
		else { Console.WriteLine("Not Object"); }
	}
}
```

- **Object**
- Not Object
- Произойдёт ошибка компиляции
- Произойдёт ошибка выполнения

17. Какой результат компиляции и выполнения следующего кода

```cs
class Program
{
	static void Main()
	{
		Console.WriteLine(1 % -2);
		Console.WriteLine(-1 % 2);
		Console.WriteLine(-1 % -2);
	}
}
```

- -1 1 -1
- -1 -1 1
- 1 1 1
- **1 -1 -1**

> Объяснение: Знак результата операции % определяется знаком её первого аргумента.

18. Каким будет результат компиляции и выполнения следующего кода

```cs
class Program
{
	static void Main()
	{
		int x = 5;
		int y = 6;
		Console.WriteLine((x == x--) && ((y == y--) && x != y) );
	}
}
```

- Произойдёт ошибка компиляции
- **True**
- False
- 5

19. Какой класс является базовым для всех классов в C#?

- System.Net
- **System.Object**
- System.Root 
- System

20. Какой результат компиляции и выполнения следующего кода

```cs
class Vehicle
{
	public void go() { Console.WriteLine("Vehicle"); }
}
class Car : Vehicle
{
	public void go() { Console.WriteLine("Car"); }
}
class Program
{
	static void Main()
	{
		Car c = new Car();
		c.go();
		Vehicle v = new Vehicle();
		v.go();
		v = c;
		v.go();
	}
}
```

- **Car Vehicle Vehicle**
- Car Vehicle Car
- Car Car Car
- Vehicle Vehicle Vehicle

21. Каким будет результат компиляции и выполнения следующего кода

```cs
class Program
{
	static void Main(string[] args)
	{
		Console.WriteLine("{0}, {1}", 1 % 3, 8 % 3);
	}
}
```

- {0}, {1}
- 1, 5
- **1, 2**
- 1, -5
- 2, 5
- 2, -5
- Произойдёт ошибка компиляции

22. Что выведет данный код?

```cs
public class Test
{
	public static void Main()
	{
		for (int i = 0; i < 4; i++)
		{
			if (i == 2) { continue; }
			if (i == 3) { break; }
			Console.Write(i);
		}
	}
}
```

- 0123
- **01**
- 1234
- 013
- 012

> Объяснение: Оператор continue передает элемент управления следующей итерации. Оператор break завершает ближайший внешний цикл или оператор switch, в котором он появляется.

23. Каким будет результат компиляции и выполнения следующего кода?

```cs
struct A
{
	public virtual void foo() { Console.WriteLine(1 == 1); }
}
class Program
{
	static void Main(string[] args)
	{
		A a = new A();
		a.foo();
	}
}
```

- False
- Ничего не будет выведено
- Произойдёт ошибка компиляции
- True

> Объяснение: Структуры не могут иметь виртуальных методов, поэтому возникнет ошибка компиляции.

24. Поддерживает ли С# перегрузку методов на основе возвращаемого значения?

- **Нет**
- Да

25. Каким будет результат компиляции и выполнения данного кода?

```cs
public class Test
{
	public static void Main()
	{
		int i, j, s = 0;

		for (i = 0, j = 4; i < j; ++i, --j )
			s += i;
		
		Console.WriteLine(s);
	}
}
```

- 0
- **1**
- 2
- 3
- Произойдёт ошибка компиляции

> Объяснение: В цикле допустимо иметь более одного счетчика.

26. Чем отличаются следующие объявления переменной: 

int[,] a; 
int[][] a;

- В первом случае -- объявление двумерного массива. Во втором -- трехмерного.
- **В первом случае объявляется обыкновенный двумерный массив. Во втором случае объявляется массив массивов, где каждая строка массива может иметь свою длину.**
- В первом случае объявляется "рваный массив", т.е. массив массивов, где каждая строка массива может иметь свою длину. Во втором случае объявляется обыкновенный двумерный массив. 
- Ничем не отличаются.

27. Укажите все правильные способы объявления массива:

- **int[] a;**
- int a[];
- **int[][] a;**
- int a[][];
- **int[,] a;**

28. Каким будет результат компиляции и выполнения следующего кода? 

```cs
class Test
{
	static void Main()
	{
		int a = 1, b = 2;
		Swap(a, b);
		Console.WriteLine("a={0}, b={1}", a, b);
	}
	
	private static void Swap(int a, int b)
	{
		int c = a;
		a = b;
		b = c;
	}
}
```

- **a=1, b=2**
- Возникнет ошибка компиляции
- Возникнет ошибка выполнения
- a=2, b=1

> Объяснение: При передаче в метод value type значений создаются их копии. Следовательно значения поменяются внутри метода, а в Main они останутся при своих значениях.

29. Какие из перечисленных выражений компилируются без ошибок:

- object a = delegate() { return 1; }
- **object a = "a";**
- Delegate a = delegate() { return 1; }
- **object a = 1;**
- int a = "a";

30. Каким будет результат компиляции и выполнения следующего кода? 

```cs
public class Test
{
	public static int i = 0;
	public static void Main()
	{
		Test t = new Test();
		t.i = 10;
		Console.WriteLine(i);
	}
}
```

- **Произойдёт ошибка компиляции**
- 0
- [0, 10]
- 10

> Объяснение: Нельзя получить доступ к статической переменной класса через экземпляр этого класса.

31. Каким будет результат компиляции и выполнения следующего кода?

```cs
public class Test
{
	public static void Main()
	{
		try
		{
			Test t = null;
			Console.WriteLine(t.ToString());
		}
		catch (Exception e)
		{
			Console.WriteLine("Exception");
		}
		catch (NullReferenceException e)
		{
			Console.WriteLine("NullReferenceException");
		}
	}
}
```

- **Произойдёт ошибка компиляции**
- Exception
- NullReferenceException
- Exception NullReferenceException 

> Объяснение: Первый catch перехватывает все исключения типа NullReferenceException, из-за этого произойдёт ошибка компиляции.

32. Что напечатает следующий код

```cs
class Program
{
	static void Main()
	{
		int i = 5;
		Console.Write(i += 1);
		Console.Write(++i);
		Console.Write(i++);
		Console.Write(i);
	}
}

- 6677
- **6778**
- 6788
- 6789

33. Что будет выведено на консоль в результате выполнения следующего кода?

```cs
public class Test
{
	public static void Main()
	{
		Console.WriteLine(test(1) + test(2) + test(3));
	}
	static int test(int a)
	{
		Console.Write(a);
		return a;
	}
}
```

- 2136
- **1236**
- 3216
- 6123

> Объяснение: Изначально отработают методы test, а уже потом отобразится их сумма.

34. Можно ли из метода некоторого класса напрямую обратиться к приватному полю другого экземпляра этого же класса? 

```cs
class A
{
	private int a = 1;
	public void Method(A other)
	{
		// обращение к приватному полю другого экземпляра этого же класса
		Console.WriteLine(other.a);
	}
}

- Только если это поле статическое
- Нет, можно обратиться только к public
- Нет, можно обратиться только к public и protected
- **Да, можно**

35. Каким будет результат компиляции и выполнения следующего кода

```cs
class Program
{
	static void Main()
	{
		Console.WriteLine(0 / 0.0);
	}
}

- Infinity
- 0
- Ошибка компиляции
- NaN
- Ошибка выполнения

36. Какой результат компиляции и выполнения следующего кода

```cs
class Program
{
	static void Main()
	{
		Console.WriteLine(" this is a test".Split(' ').Length);
	}
}
```

- 6
- **5**
- 4
- 3
- Ошибка компиляции

> Объяснение: Split разобъет строку на следующие части: [, this, is, a, test]

37. Каким будет результат компиляции и выполнения следующего кода

```cs
public class Test
{
	public static void Main()
	{
		int i = 3;
		switch (i)
		{
			case 3:
			case 2: Console.WriteLine(2); break; 
			case 1: 
			default: Console.WriteLine(0);
		}
	}
}
```

- 2 0
- 0
- 2
- **Произойдёт ошибка компиляции**

> Объяснение: В C# в конструкции switch не допускается проход (fall through) в следующий раздел без операторов goto case или goto default. В конце каждого раздела требуется наличие оператора break или другой передачи управления (return, throw, continue). Исключением являются пустые разделы. Эти правила также относятся и к последнему разделу.

38. Среди перечисленных конструкций C# укажите объявление метода

- string this [int i] { get { return "Method"; }}
- string Method { get { return "Method"; } }
- string Method;
- **string Method () { return "Method";}**

39. Каким будет результат компиляции и выполнения следующего кода? 

```cs
interface IA
{
	void SomeWork();
}
class B : IA
{
	void IA.SomeWork()
	{
		Console.WriteLine("B");
	}
}
public class Test
{
	public static void Main()
	{
		B b = new B();
		b.SomeWork();
	}
}
```

- B
- Ничего не будет выведено
- **Произойдёт ошибка компиляции**

> Объяснение: Когда метод реализован явно, обратиться к нему можно только через ссылку на этот интерфейс.

40. Какие утверждения из перечисленных верны о структурах?

- Структуры могут наследоваться от перечислений
- Структуры могут наследоваться от структур
- **Структуры могут реализовывать интерфейсы**
- Классы могут наследоваться от структур

41. Каким будет результат компиляции и выполнения следующего кода

```cs
class Program
{
	static void Main()
	{
		int i = 1;
		Console.WriteLine("i = {0}", i++);
	}
}
```

- i = 2
- i = {0} 1
- **i = 1**
- произойдёт ошибка компиляции
- i = {0} 2

> Объяснение: Выражение i++ произведёт увелечение значения переменной i на единицу, но вернёт предыдущее значение.

42. Скомпилируется ли данный код как часть некоторого метода?

```cs
try
{
	Console.WriteLine("TRY");
	throw new Exception("EXCEPTION");
}
catch (Exception)
{
	Console.WriteLine("CATCH");
}
```

- **Да**
- Нет

> Объяснение: В блоке catch не обязательно указывать идентификатор исключения

43. Какой модификатор доступа по умолчанию даётся классу, описанному в namespace?

- protected
- **internal**
- public
- Ни один из перечисленных
- private

> Объяснение: Невложенный класс - класс, который находится непосредственно в namespace. Такой класс может иметь 2 модификатора доступа - public и internal. По умолчанию класс имеет модификатор internal.

44. Каким будет результат компиляции и выполнения следующего кода

```cs
class Program
{
	static bool method1()
	{
		Console.WriteLine("method1");
		return true;
	}
	static bool method2()
	{
		Console.WriteLine("method2");
		return false;
	}
	static void Main()
	{
		if (method1() || method2())
			Console.WriteLine(true);
		else
			Console.WriteLine(false);
	}
}
```

- method1 method2 False
- method1 method2 True
- method1 False
- **method1 True**
- method2 False
- method2 True

> Объяснение: В начале будет выполнен метод method1, он вернёт true, поэтому вычислять результат method2 не будет смысла и будет выведено True.

45. Укажите все правильные способы объявления массива в результате которых массив будет корректно инициализирован
	 
- **int[] someArray = new int[] {1,2,3,4};**
- **int[] someArray = new int[4];**
- int someArray[] = new int[4];
- **int[] someArray = {1,2,3,4};**
- int[4] someArray;
- **int[] someArray = new int[4] {1,2,3,4};**

46. Что напечатает следующий код:

```cs
public static void Main()
{
	Int32 v = 5;
	Object o = v;
	v = 123;
	Console.WriteLine(v + ", " + (Int32) o);
}
```
	 
- Возникнет ошибка приведения типов
- **123, 5**
- 5, 5
- 123, 123

47. Как могут инициализироваться readonly поля экземпляра класса?
	 
- с помощью свойств
- **в конструкторе**
- **при объявлении**
- в instance методах
- в статических методах

48. Укажите ошибки в коде:

```cs
сlаss Nаmе
{
	#rеgiоn Cоnstruсtоr
	рubliс Nаmе(string nаmе)
	{
		this.Nаmе = nаmе;
	}
	#еndrеgiоn
	#rеgiоn Рrореrtiеs
	/*1*/ рubliс string Nаmе { рrivаtе sеt; gеt; }
	#еndrеgiоn
	#rеgiоn Меthоds
	stаtiс vоid Маin(string[] аrgs)
	{
		/*2*/ Nаmе n = nеw Nаmе("Hеlо Wrоld");
		/*3*/ n.Disроsе(); // освобождаем память.
	}
	#еndrеgiоn
}
	 
- **Строка /*1*/ Класс Name не может содержать свойство Name**
- Строка /*2*/ : ошибки в фразе "Helo Wrold"
- Комментарии вида /* __ */ недопустимы
- **Строка /*3*/ - вызов Dispose для переменной n невозможен.**

> Пояснение: Hello World является строковым выражением, его синтаксис не проверяется. Класс не может содержать членов с именем, совпадающим с именем самого класса. Dispose не объявлен.

49. Какими способами из перечисленных может быть перегружен метод в C#?
	 
- **другой тип параметров**
- **другое количество параметров**
- другой тип возвращаемого значения
- другое имя метода

50. Отметьте все верные утверждения о классе SomeClass:

```cs
internal sealed class SomeClass
{
	public void SomeMethod() { }
}
```

- экземпляр SomeClass нельзя создать вызовом new SomeClass(), так как у класса не определен открытый конструктор без параметров
- SomeClass является значимым типом (value-type)
- **у SomeClass не может быть наследников**
- **SomeClass наследуется от System.Object**
- SomeClass могут наследовать другие классы
- SomeClass может использоваться внешними сборками
- **SomeClass доступен для использования только внутри сборки, в которой он определён**

> Пояснение: 
1. SomeClass неявно наследует System.Object
2. у SomeClass не может быть наследников (модификатор sealed означает, что класс не может порождать наследников)
3. SomeClass доступен для использования только внутри сборки, в которой он определён (модификатор internal определяет класс, как внутренний)
4. явное определение конструктора без параметров для ссылочного типа необязательно - компилятор создаёт его, если конструктор не определён явно
5. SomeClass - ссылочный тип, т.к. унаследован от object

51. Что верно по отношению к данному фрагменту кода:

```cs
for (;;)
	Console.WriteLine("test");
```
	 
- Код не скомпилируется, так как цикл объявлен неправильно
- Код скомпилируется, но тело цикла не выполнится ни разу
- **Код скомпилируется и будет бесконечно печатать слово "test"**
- Код скомпилируется и ничего не напечатает

52. Укажите правильный порядок секций catch при обработке различных типов исключений.
	 
- От наименее вероятных к наиболее вероятным.
- От наиболее вероятных к наименее вероятным.
- От общих к специфичным.
- **От специфичных к общим.**

53. Что будет выведено на консоль в результате выполнения следующего кода:

```cs
class Program
{
	static void Main(string[] args)
	{
		Console.WriteLine(Test.Foo().ToString());
		Console.ReadLine();
	}
}
class Test
{
	public static int Foo()
	{
		return null;
	}
}
```
	 
- пустая строка
- NULL
- System.Null
- **Возникнет ошибка компиляции**

> Пояснение: null в данном случае не является типом возвращаемого значения

54. В каких строках кода допущены ошибки?

```cs
class A { }
interface Inner { }
struct S : A, Inner  //1
{
	int num = 10;    //2
	public S() { }   //3
	static S() { }   //4
}
class Program
{
	static void Main(string[] args)
	{
		S obj = new S(); //5
	}
}
```
	 
- **1**
- 2
- 3
- 4
- 5

> Пояснение: 
1. структуры не поддерживают классическое наследование, а A - это имя класса
2. поля структур нельзя инициализировать при объявлении
3. в структурах нельзя определять конструкторы экземпляров по умолчанию

55. Будет ли компилироваться данный фрагмент кода?

```cs
try
{
	FileStream F = new FileStream("myfile.txt");
	string s = F.ReadLine();
}
catch (IOException) { }
finally
{
	F.Close();
}
```
	 
- Да
- **Нет**

> Пояснение: Компилятор ругнется на то, что переменная F не определена в месте вызова F.Close()

56. В каких строках содержатся ошибки компиляции?    

```cs
public class Foo
{
	private double field;
	public double A
	{
		get { return field + 10.0; }        // 1
		set { field = value - 10.0; }       // 2
	}
	public int B { get; private set; }      // 3
}
public static class Program
{
	public static void Main()
	{
		Foo f = new Foo();
		double a = f.A;     // 4
		int b = f.B;        // 5
		f.B = 10;           // 6
	}
}
```

- 1
- 2
- 3
- 4
- 5
- **6**
- Код компилируется без ошибок

> Пояснение: А - свойство с методами доступа для чтения и для записи. B - автоматически реализуемое свойство с асимметричными методами доступа. Для записи свойство доступно только членам класса
Подробности здесь: http://msdn.microsoft.com/ru-ru/library/x9fsa0sw(VS.90).aspx

57. В каких строках содержатся ошибки компиляции    

```cs
public class Foo
{
	public Action A;
	public Action B { get; private set; }
	public event Action C;
}
public static class Program
{
	public static void Main()
	{
		Foo foo = new Foo();
		foo.A += () => { }; // 1
		foo.A();            // 2
		foo.B += () => { }; // 3
		foo.B();            // 4
		foo.C += () => { }; // 5
		foo.C();            // 6
	}
}
```
	 
- Код скомпилируется успешно, в нём нет ошибок
- 1
- 2
- 3
- 4
- 5
- **6**

> Пояснение:
A - делегат, к нему можно добавлять методы и его можно вызывать;
B - делегат, доступный через свойство с закрытым методом записи. Из другого класса делегат B можно только вызвать, изменить - нельзя;
C - событие. К нему можно добавлять методы. Вызвать событие можно только из класса в котором оно объявлено.
http://msdn.microsoft.com/ru-ru/library/8627sbea(VS.90).aspx

58. Каков будет результат при выполнении следующего кода

```cs
public abstract class A
{
	public virtual string Print(){return "A";}
}
public class B:A
{
	public virtual new string Print(){return "B";}
}
public class C:B
{
	public override string Print(){return "C";}
} 

A ac = new C();
Console.WriteLine(ac.Print());

```
	 
- **A**
- B
- C
- Ошибка компиляции

59. Какие модификаторы доступа из перечисленных по умолчанию даются классу, описанному в namespace?
	 
- public
- private
- protected
- **internal**
- Ни один из перечисленных

> Пояснение: Невложенный класс - класс, который находится непосредственно в namespace. Такой класс может иметь 2 модификатора доступа - public и internal. По умолчанию класс имеет модификатор internal.

60. Какой будет результат компиляции и выполнения данного кода:

```cs
public static void Main(string[] args)
{
	int k = 1;
	Console.WriteLine(k++ + ++k);
}
```
	 
- **4**
- 5
- 2
- 3

> Пояснение: (k++ + ++k):
1. k++=1 - результат выражения, но после k++ увеличивает значение k на 1, т.е. k=2. Таким образом имеем (1 + ++k), где k=2.
2. (1 + (2+1))=4

61. Напишите название функции, которая является точкой входа в любую программу на C#.

Правильные ответы: **Main**

> Пояснение: Ответ Main() или main не являются верными. Название функции - Main

62. В определении каких методов класса допущены ошибки?

```cs
public class Foo
{
	public void M1(int[] p1, int[] p2) { }
	public void M2(int[] p1, params int[] p2) { }
	public void M3(params int[] p1, int[] p2) { }
	public void M4(params int[] p1, params int p2) { }
}
```
	 
- Ошибок нет, всё определено корректно
- M1
- M2
- **M3**
- **M4**

> Пояснение: После параметра с ключевым словом params, дополнительные параметры не допускаются. Не допускается использование ключевого слова params в объявлении метода более одного раза http://msdn.microsoft.com/ru-ru/library/w5zay9db(VS.90).aspx
