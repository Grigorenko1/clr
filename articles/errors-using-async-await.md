﻿# Частые Ошибки при Работе с `async/await`.

1. **Лишний `async/await`**

Использование ключевых слов `async/await` приводит к неявному выделению памяти под конечный автомат (https://t.me/NetDeveloperDiary/249), который отвечает за организацию асинхронных вызовов. 
Когда ожидаемое выражение является единственным или последним оператором в функции, его можно пропустить. Однако с этим есть несколько проблем, о которых вам следует знать (https://t.me/NetDeveloperDiary/680).

❌ Неверно
```cs
async Task DoSomethingAsync()
{
  await Task.Yield();
}
```

✅ Верно
```cs
Task DoSomethingAsync()
{
  return Task.Yield();
}
```

2. **Вызов синхронного метода внутри асинхронного метода**

Если мы используем асинхронный код, мы всегда должны использовать асинхронные методы, если они существуют. Многие API предлагают асинхронные аналоги своих хорошо известных синхронных методов.

❌ Неверно
```cs
async Task DoAsync(Stream file, byte[] buffer)
{
  file.Read(buffer, 0, 10);
}
```

✅ Верно
```cs
async Task DoAsync(Stream file, byte[] buffer)
{
  await file.ReadAsync(buffer, 0, 10);
}
```

3. **Использование `async void`**

Есть две причины, по которым `async void` вреден:
- Вызывающий код не может ожидать метод `async void`.
- Невозможно обработать исключение, вызванное этим методом. Если возникает исключение, ваш процесс падает!

Вы всегда должны использовать `async Task` вместо `async void`, если только это не обработчик событий, но тогда вы должны гарантировать себе, что метод выбросит исключения.

❌ Неверно
```cs
async void DoSomethingAsync()
{
  await Task.Yield();
}
```

✅ Верно
```cs
async Task DoSomethingAsync()
{
  await Task.Yield();
}
```

4. **Неподдерживаемые асинхронные делегаты**

Если передать асинхронную лямбда-функцию в качестве аргумента типа `Action`, компилятор сгенерирует метод `async void`, недостатки которого были описаны выше. Есть два решения этой проблемы:
- изменить тип параметра с `Action` на `Func<Task>`, 
- реализовать этот делегат синхронно.

Стоит отметить, что некоторые API-интерфейсы уже предоставляют аналоги своих методов, которые принимают `Func<Task>`.

```cs
void DoSomething()
{
  Callback(async() =>
  {
    await Task.Yield();
  });
}
```

❌ Неверно
`void Callback(Action action) {…}`

✅ Верно
`void Callback(Func<Task> action) {…}`

5. **Не ожидать `Task` в выражении `using`**

`System.Threading.Tasks.Task` реализует интерфейс `IDisposable`. Вызов метода, возвращающего `Task`, непосредственно в выражении `using` приводит к удалению задачи в конце блока `using`, 
что никогда не является ожидаемым поведением.

❌ Неверно
`using (CreateDisposableAsync()) {…}`

✅ Верно
`using (await CreateDisposableAsync()) {…}`

6. **Не ожидать `Task` в блоке `using`**

Если мы пропустим ключевое слово `await` для асинхронной операции внутри блока `using`, то объект может быть удалён до завершения асинхронного вызова. 
Это приведёт к неправильному поведению и очень часто заканчивается исключением времени выполнения, сообщающим о том, что мы пытаемся работать с уже удалённым объектом.

❌ Неверно
```cs
private Task<int> DoSomething()
{
  using (var service = CreateService())
  {
    return service.GetAsync();
  }
}
```

✅ Верно
```cs
private async Task<int> DoSomething()
{
  using (var service = CreateService())
  {
    return await service.GetAsync();
  }
}
```

7. **Не ожидать результата асинхронного метода**

Отсутствие ключевого слова `await` перед асинхронной операцией приведет к завершению метода до завершения данной асинхронной операции. 
Метод будет вести себя непредсказуемо, и результат будет отличаться от ожиданий. Ещё хуже, если неожидаемое выражение выбросит исключение. 
Оно остается незамеченным и не вызовет сбоя процесса, что ещё больше затруднит его обнаружение. 
Вы всегда должны ожидать асинхронное выражение или присвоить возвращаемую задачу переменной и убедиться, что в итоге она где-то ожидается.

❌ Неверно
````cs
async Task DoSomethingAsync()
{
  await DoSomethingAsync1(); 
  DoSomethingAsync2();
  await DoSomethingAsync3(); 
}
```

✅ Верно
```cs
async Task DoSomethingAsync()
{
  await DoSomethingAsync1(); 
  await DoSomethingAsync2();
  await DoSomethingAsync3();
}
```

8. **Синхронные ожидания**

Если вы хотите ожидать асинхронное выражение в синхронном методе, вы должны переписать всю цепочку вызовов на асинхронную. 
Кажется, что более легкое решение - вызвать `Wait` или `Result` для возвращаемой задачи. 
Но это будет стоить вам заблокированного потока и может привести к взаимной блокировке. Сделайте всю цепочку асинхронной. 
В консольном приложении допустим асинхронный метод `Main`, в контроллерах ASP.NET – асинхронные методы действия.

❌ Неверно
```cs
void DoSomething()
{
  Thread.Sleep(1);
  Task.Delay(2).Wait();
  var result1 = GetAsync().Result;
  var result2 = GetAsync().GetAwaiter().GetResult();
  var unAwaitedResult3 = GetAsync();
  var result3 = unAwaitedResult3.Result;
}
```

✅ Верно
```cs
async Task DoSomething()
{
  await Task.Delay(1);
  await Task.Delay(2);    
  var result1 = await GetAsync();
  var result2 = await GetAsync();
}
```

9. **Отсутствие ConfigureAwait(bool)**

По умолчанию, когда мы ожидаем асинхронную операцию, продолжение планируется с использованием захваченного `SynchronizationContext` или `TaskScheduler`. 
Это приводит к дополнительным затратам и может привести к взаимной блокировке, особенно в `WindowsForms`, `WPF` и старых приложениях ASP.NET. 
Вызывая `ConfigureAwait(false)`, мы гарантируем, что текущий контекст игнорируется при вызове продолжения. 
Подробнее о `ConfigureAwait` в серии постов с тегом #AsyncAwaitFAQ

❌ Неверно
```cs
async Task DoSomethingAsync()
{
  await DoSomethingElse();
}
```

✅ Верно
```cs
async Task DoSomethingAsync()
{
  await DoSomethingElse().ConfigureAwait(false);
}
```

10. **Возврат `null` из метода, возвращающего `Task`**

Возврат `null` значения из синхронного метода типа `Task/Task<>` приводит к исключению `NullReferenceException`, если кто-то ожидает этот метод. 
Чтобы этого избежать, всегда возвращайте результат этого метода с помощью помощников `Task.CompletedTask` или `Task.FromResult<T>(null)`.

❌ Неверно
```cs
Task DoAsync()
{
  return null;
}
Task<object> GetSomethingAsync()
{
  return null;
}
Task<HttpResponseMessage> TryGetAsync(HttpClient httpClient)
{
  return httpClient?.GetAsync("/some/endpoint");
}
```

✅ Верно
```cs
Task DoAsync()
{
  return Task.CompletedTask;
}
Task<object> GetSomethingAsync()
{
  return Task.FromResult<object>(null);
}
Task<HttpResponseMessage> TryGetAsync(HttpClient httpClient)
{
  return httpClient?.GetAsync("/some/endpoint")
    ?? Task.FromResult(default(HttpResponseMessage));
}
```

11. **Передача токена отмены**

Если вы забудете передать токен отмены, это может привести к большим проблемам. 
Запущенная длительная операция без токена отмены может заблокировать действие по остановке приложения или привести к нехватке потоков при большом количестве отменённых веб-запросов. 
Чтобы избежать этого, всегда создавайте и передавайте токен отмены методам, которые его принимают, даже если это необязательный параметр.

❌ Неверно
```cs
public async Task<string> GetSomething(HttpClient http)
{
  var response = await http.GetAsync(new Uri("/endpoint/")); 
  return await response.Content.ReadAsStringAsync();
}
```

✅ Верно
```cs
public async Task<string> GetSomething(HttpClient http, CancellationToken ct)
{
  var response = await http.GetAsync(new Uri("/endpoint/"), ct);
  return await response.Content.ReadAsStringAsync();
}
```

12. **Использование токена отмены с `IAsyncEnumerable`**

Эта ошибка похожа на предыдущую, но относится исключительно к `IAsyncEnumerable` и её довольно легко допустить. 
Это может быть неочевидно, но передача токена отмены в асинхронный перечислитель выполняется путем вызова метода `WithCancellation()`.

❌ Неверно
```cs
async Task Iterate(IAsyncEnumerable<string> e)
{
  await foreach (var item in e)
  {
   …
  }
}
```

✅ Верно
```cs
async Task Iterate(IAsyncEnumerable<string> e, CancellationToken ct)
{
  await foreach (var item in e.WithCancellation(ct))
  {
   …
  }
}
```

13. **Имена асинхронных методов должны заканчиваться на `Async`**

И наоборот, имена синхронных методов не должны заканчиваться на `Async`. Так обычно происходит при невнимательном рефакторинге. 
Это не строгое обязательство и не ошибка. Вообще, это соглашение об именовании имеет смысл только тогда, когда класс предоставляет как синхронные, так и асинхронные версии метода.

❌ Неверно
```cs
async Task DoSomething()
{
  await Task.Yield();
}
```

✅ Верно
```cs
async Task DoSomethingAsync()
{
  await Task.Yield();
}
```

14. **Злоупотребление `Task.Run` в простых случаях**

Для предварительно вычисленных или легко вычисляемых результатов нет необходимости вызывать `Task.Run`, который поставит задачу в очередь пула потоков, а задача немедленно завершится. 
Вместо этого используйте `Task.FromResult`, чтобы создать задачу, обёртывающую уже вычисленный результат.

❌ Неверно
```cs
public Task<int> AddAsync(int a, int b)
{
  return Task.Run(() => a + b);
}
```

✅ Верно
```cs
public Task<int> AddAsync(int a, int b)
{
  return Task.FromResult(a + b);
}
```

15. **`Await` лучше, чем `ContinueWith`**

Хотя методы продолжений по-прежнему можно использовать, обычно рекомендуется использовать `async/await` вместо `ContinueWith`. 
Кроме того, надо отметить, что `ContinueWith` не захватывает `SynchronizationContext` и поэтому семантически отличается от `async/await`.

❌ Неверно
```cs
public Task<int> DoSomethingAsync()
{
  return CallDependencyAsync().ContinueWith(task =>
  {
    return task.Result + 1;
  });
}
```

✅ Верно
```cs
public async Task<int> DoSomethingAsync()
{
  var result = await CallDependencyAsync();
  return result + 1;
}
```

16. **Синхронизация асинхронного кода в конструкторах**

Конструкторы синхронны. Если вам нужно инициализировать некоторую логику, которая может быть асинхронной, 
использование `Task.Result` для синхронизации в конструкторе может привести к истощению пула потоков и взаимным блокировкам. Лучше использовать статическую фабрику.

```cs
public interface IRemoteConnFactory
{
  Task<IRemoteConn> ConnectAsync();
}

public interface IRemoteConn { … }
```

❌ Неверно
```cs
public class Service : IService
{
  private readonly IRemoteConn _conn;

  public Service(IRemoteConnFactory cf)
  {
    _conn = cf.ConnectAsync().Result;
  }
}
```

✅ Верно
```cs
public class Service : IService
{
  private readonly IRemoteConn _conn;

  private Service(IRemoteConn conn)
  {
    _conn = conn;
  }

  public static async Task<Service> CreateAsync(IRemoteConnFactory cf)
  {
    return new Service(await cf.ConnectAsync());
  }
}
…
var cf = new ConnFactory();
var srv = await Service.CreateAsync(cf);
```

17. **Анализаторы кода для асинхронного программирования**
Большинство ошибок, описанных в предыдущих постах, можно обнаружить с помощью анализаторов кода, представленных в следующих пакетах. Многие из них не ограничены поиском ошибок в асинхронном коде, поэтому для каждого приведены правила, проверяющие перечисленные ранее ошибки.

1. [Microsoft.CodeAnalysis.FxCopAnalyzers](https://www.nuget.org/packages/Microsoft.CodeAnalysis.FxCopAnalyzers/)
CA2007 - Consider calling ConfigureAwait on the awaited task
CA2008 - Do not create tasks without passing a TaskScheduler
[Полный список правил](https://github.com/dotnet/roslyn-analyzers/blob/master/src/Microsoft.CodeAnalysis.FxCopAnalyzers/Microsoft.CodeAnalysis.FxCopAnalyzers.md)

2. [Microsoft.VisualStudio.Threading.Analyzers](https://www.nuget.org/packages/Microsoft.VisualStudio.Threading.Analyzers/)
VSTHRD002 - Avoid problematic synchronous waits
VSTHRD100 - Avoid async void methods
VSTHRD101 - Avoid unsupported async delegates
VSTHRD107 - Await Task within using expression
VSTHRD111 - Use .ConfigureAwait(bool)
VSTHRD112 - Implement System.IAsyncDisposable
VSTHRD114 - Avoid returning null from a Task-returning method
VSTHRD200 - Use Async naming convention
[Полный список правил](https://github.com/microsoft/vs-threading/blob/master/doc/analyzers/index.md)

3. [AsyncFixer](https://www.nuget.org/packages/AsyncFixer)
AsyncFixer01 - Unnecessary async/await usage
AsyncFixer03 - Fire & forget async void methods
AsyncFixer04 - Fire & forget async call inside a using block
[Полный список правил](https://www.nuget.org/packages/AsyncFixer)

4. [Roslyn.Analyzers](https://www.nuget.org/packages/Roslyn.Analyzers/)
ASYNC0001 - Asynchronous method names should end with Async
ASYNC0002 - Non asynchronous method names shouldn’t end with Async
ASYNC0003 - Avoid void returning asynchronous method
ASYNC0004 - Use ConfigureAwait(false) on await expression 
[Полный список правил](https://roslyn-analyzers.readthedocs.io/en/latest/repository.html#analyzers-in-the-repository)

5. [Meziantou.Analyzer](https://www.nuget.org/packages/Meziantou.Analyzer/)
MA0004 - Use .ConfigureAwait(false)
MA0022 - Return Task.FromResult instead of returning null
MA0032 - Use an overload that have cancellation token
MA0040 - Flow the cancellation token when available
MA0045 - Do not use blocking call (make method async)
MA0079 - Flow a cancellation token using .WithCancellation()
MA0080 - Specify a cancellation token using .WithCancellation()
[Полный список правил](https://github.com/meziantou/Meziantou.Analyzer/tree/master/docs)

6. [Roslynator](https://www.nuget.org/packages/Roslynator.Analyzers/)
RCS1046 - Asynchronous method name should end with Async
RCS1047 - Non-asynchronous method name should not end with Async 
RCS1090 - Call ConfigureAwait(false)
RCS1174 - Remove redundant async/await 
RCS1210 - Return Task.FromResult instead of returning null
RCS1229 - Use async/await when necessary
[Полный список правил](https://github.com/JosefPihrt/Roslynator/blob/master/src/Analyzers/README.md)

7. [Asyncify](https://www.nuget.org/packages/Asyncify/)
AsyncifyInvocation - This invocation could benefit from the use of Task async
AsyncifyVariable - This variable access could benefit from the use of Task async