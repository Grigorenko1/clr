﻿# Потоки
Процессом называется набор ресурсов, используемый отдельным экземпляром приложения. Каждому процессу выделяется виртуальное адресное пространство; это гарантирует, что код и данные одного процесса будут недоступны для другого. Это делает приложения отказоустойчивыми, поскольку при таком подходе один процесс не может повредить код или данные другого. Код и данные ядра также недоступны для процессов; а значит, код приложений не в состоянии повредить код или данные операционной системы. Это упрощает работу конечных пользователей. Система становится также более безопасной, потому что код произвольного приложения не имеет доступа к именам пользователей, паролям, информации кредитной карты или иным конфиденциальным данным, с которыми работают другие приложения или сама операционная система. 

# Уровни изолированности транзакций

| Уровень | Возможная реализация |
| :------ | -------------------- |
| Read uncommitted | Уровень, имеющий самую плохую согласованность данных, но самую высокую скорость выполнения транзакций. Название уровня говорит само за себя — каждая транзакция видит незафиксированные изменения другой транзакции (феномен грязного чтения). |
| Read committed | Для этого уровня параллельно исполняющиеся транзакции видят только зафиксированные изменения из других транзакций. Таким образом, данный уровень обеспечивает защиту от грязного чтения. |
| Repeatable read | Уровень, позволяющий предотвратить феномен неповторяющегося чтения. Т.е. мы не видим в исполняющейся транзакции измененные и удаленные записи другой транзакцией. Но все еще видим вставленные записи из другой транзакции. Чтение фантомов никуда не уходит. |
| Serializable | Уровень, при котором транзакции ведут себя как будто ничего более не существует, никакого влияния друг на друга нет. В классическом представлении этот уровень избавляет от эффекта чтения фантомов. |

https://habr.com/ru/post/469415/
