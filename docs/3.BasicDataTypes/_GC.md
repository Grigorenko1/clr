# Автоматическое управление памятью
### Управляемая куча
Любая программа использует ресурсы — файлы, буферы в памяти, пространство экрана, сетевые подключения, базы данных и т. п. В объектно-ориентированной среде каждый тип идентифицирует некий доступный этой программе ресурс. Чтобы им воспользоваться, должна быть выделена память для представления этого типа. Для доступа к ресурсу вам нужно: 

1. Выделить память для типа, представляющего ресурс (обычно это делается при помощи оператора new в C#). 
2. Инициализировать выделенную память, установив начальное состояние ресурса и сделав его пригодным к использованию. За установку начального состояния типа отвечает его конструктор. 
3. Использовать ресурс, обращаясь к членам его типа (при необходимости операция может повторяться). 
4. В рамках процедуры очистки уничтожить состояние ресурса. 
5. Освободить память. За этот этап отвечает исключительно уборщик мусора.

Эта простая на первый взгляд парадигма стала одним из основных источников ошибок у программистов, которым приходится вручную управлять памятью. 

Если вы пишете код, безопасный по отношению к типам, повреждение памяти в ваших приложениях невозможно. Утечки памяти остаются теоретически возможными, но они не происходят в стандартной ситуации. Как правило, утечки памяти возникают из-за того, что приложение хранит объекты в коллекции, но не удаляет их, когда они становятся ненужными. 

Ситуация дополнительно упрощается тем, что для большинства типов, регулярно используемых разработчиками, шаг 4 (уничтожение состояния ресурса) не является обязательным. Таким образом, управляемая куча, кроме ликвидации уже упомянутых ошибок, также предоставляет разработчику простую модель программирования: программа выделяет и инициализирует ресурс, после чего использует его так долго, сколько понадобится. Для большинства типов очистка ресурсов не нужна, память просто освобождается уборщиком мусора. 

При использовании экземпляров типов, требующих специальной очистки, модель программирования остается такой же простой. Впрочем, иногда очистка ресурса должна выполняться как можно раньше, не дожидаясь вмешательства уборщика мусора. В таких классах можно вызвать дополнительный метод (называемый Dispose), чтобы очистка была выполнена по вашему собственному расписанию. 

##### Выделение ресурсов из управляемой кучи
В CLR память для всех ресурсов выделяется из так называемой управляемой кучи (managed heap). При инициализации процесса CLR резервирует область адресного пространства под управляемую кучу, а также указатель, который определяет, где в куче будет выделена память для следующего объекта, и изначально указывает на базовый адрес этой зарезервированной области адресного пространства. 

По мере заполнения области объектами CLR выделяет новые области, вплоть до заполнения всего адресного пространства. 

При выполнении оператора C# new среда CLR: 
1. подсчитывает количество байтов, необходимых для размещения полей типа (и всех полей, унаследованных от базового типа); 
2. прибавляет к полученному значению количество байтов, необходимых для размещения системных полей объекта. У каждого объекта есть пара таких полей: указатель на объект-тип и индекс блока синхронизации. В 32-разрядных приложениях для каждого из этих полей требуется 32 бита, что увеличивает размер каждого объекта на 8 байт, а в 64-разрядных приложениях каждое поле занимает 64 бита, добавляя к каждому объекту 16 байт; 
3. проверяет, хватает ли в зарезервированной области байтов на выделение памяти для объекта (при необходимости передает память). Если в управляемой куче достаточно места для объекта, ему выделяется память, начиная с адреса, на который ссылается указатель NextObjPtr, а занимаемые им байты обнуляются. Затем вызывается конструктор типа (передающий NextObjPtr в качестве параметра `this`), и оператор new возвращает ссылку на объект. Перед возвратом этого адреса NextObjPtr переходит на первый адрес после объекта, указывая на адрес, по которому в куче будет помещен следующий объект. 

Для управляемой кучи выделение памяти для объекта сводится к простому увеличению указателя — эта операция выполняется почти мгновенно. Во многих приложениях объекты, выделяемые примерно в одно время, тесно связаны друг с другом, к тому же часто к ним обращаются примерно в одно время.

В среде, поддерживающей уборку мусора, новые объекты располагаются в памяти непрерывно, что повышает производительность за счет близкого расположения ссылок. В частности, это значит, что рабочий набор процесса будет меньше, чем у подобного приложения, работающего в неуправляемой среде. Также, скорее всего, все объекты, используемые в программе, уместятся в кэше процессора. Приложение сможет получать доступ к этим объектам с феноменальной скоростью, так как процессор будет выполнять большинство своих операций без кэш-промахов, замедляющих доступ к оперативной памяти.

Итак, пока складывается впечатление, что управляемая куча обладает превосходными характеристиками быстродействия. И все же это описание предполагает, что память всегда бесконечна, а CLR всегда может выделить блок для нового объекта. Конечно, это не так, поэтому управляемой куче необходим механизм уничтожения объектов, которые перестали быть нужными приложению. Таким механизмом является уборка мусора (Garbage Collection, GC).

##### Алгоритм уборки мусора
Когда приложение вызывает оператор new для создания объекта, оставшегося адресного пространства может не хватить для выделения памяти под объект. В таком случае CLR выполняет уборку мусора.

Для управления сроком жизни объектов в некоторых системах используется алгоритм подсчета ссылок. В системах с подсчетом ссылок каждый объект в куче содержит внутреннее поле с информацией о том, сколько «частей» программы в настоящее время используют данный объект. Когда каждая «часть» переходит к точке кода, в которой объект становится недоступным, она уменьшает поле счетчика объекта. Когда значение счетчика уменьшается до 0, объект удаляется из памяти. К сожалению, в системах с подсчетом ссылок возникают серьезные проблемы с циклическими ссылками. 

Из-за проблем с алгоритмами, основанными на подсчете ссылок, CLR вместо этого использует **алгоритм отслеживания ссылок**.

Когда среда CLR запускает уборку мусора, она сначала приостанавливает все программные потоки в процессе. Тем самым предотвращается обращение к объектам и возможное изменение состояния во время их анализа CLR. Затем CLR переходит к этапу уборки мусора, называемому маркировкой (marking). CLR перебирает все объекты в куче, задавая биту в поле индекса блока синхронизации значение 0. Это означает, что все эти объекты могут быть удалены. Затем CLR проверяет все активные корни и объекты, на которые они ссылаются. Если корень содержит `null`, CLR игнорирует его и переходит к следующему корню. Если корень ссылается на объект, в поле индекса блока синхронизации устанавливается бит — это и есть признак маркировки объекта. После маркировки объекта CLR проверяет все корни в этом объекте и маркирует объекты, на которые они ссылаются. Встретив уже маркированный объект, уборщик мусора останавливается, чтобы избежать возникновения бесконечного цикла в случае циклических ссылок. 

На рис. показана куча с несколькими объектами, в которой корни приложения напрямую ссылаются на объекты А, С, D и F. Все эти объекты маркируются. При маркировке объекта D уборщик мусора обнаруживает, что в этом объекте есть поле, ссылающееся на объект H, поэтому объект H также помечается. Затем уборщик продолжает рекурсивный просмотр всех достижимых объектов.

![gc_1](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_1.png "Рис. 1. Управляемая.куча.до.уборки.мусора")

После проверки всех корней куча содержит набор маркированных и немаркированных объектов. Маркированные объекты переживут уборку мусора, потому что на них ссылается хотя бы один объект; можно сказать, что они достижимы из кода приложения. Немаркированные объекты недостижимы, потому что в приложении не существует корня, через который приложение могло бы к ним обратиться. 

Теперь, когда CLR знает, какие объекты должны остаться, а какие можно удалить, начинается следующая фаза уборки мусора, называемая сжатием (compacting phase). В этой фазе CLR перемещает вниз все «немусорные» объекты, чтобы они занимали смежный блок памяти. Перемещение имеет много преимуществ. 

-  оставшиеся объекты будут находиться поблизости друг от друга; это приводит к сокращению размера рабочего набора приложения, а следовательно, повышает производительность обращения к этим объектам в будущем.
-  свободное пространство тоже становится непрерывным, что позволяет освободить эту область адресного пространства
-  сжатие позволяет избежать проблем фрагментации адресного пространства при использовании управляемой кучи.

После перемещения в памяти все ссылки на «выжившие» объекты из корней указывают на прежнее местонахождение объекта в памяти, а не на тот адрес, по которому объект был перемещен. Если возобновить выполнение потоков на этой стадии, потоки обратятся по старым адресам, что приведет к некорректному использованию памяти. Разумеется, этого допускать нельзя, поэтому в фазе сжатия CLR вычитает из каждого корня количество байт, на которое объект был сдвинут вниз в памяти. Тем самым гарантируется, что каждый корень будет ссылаться на тот же объект, что и прежде; просто сейчас этот объект оказался в другом месте памяти. После сжатия памяти кучи в указатель NextObjPtr управляемой кучи заносится первый адрес за последним объектом, не являющимся мусором. По этому адресу следующий новый объект будет размещен в памяти. После завершения фазы сжатия CLR возобновляет выполнение потоков приложения, а они обращаются к объектам так, словно никакой уборки мусора и не было.

Если CLR не удается освободить память в результате уборки мусора, а в процессах не осталось адресного пространства для выделения нового сегмента, значит, свободная память процесса полностью исчерпана. В этом случае попытка выделения новой памяти оператором `new` приведен к выдаче исключения `OutOfMemoryException`. Ваше приложение может перехватить это исключение и восстановиться после него, но большинство приложений не пытается это делать; вместо этого исключение превращается в необработанное, Windows завершает процесс, а затем освобождает всю память, использованную процессом. 

Программист должен извлечь для себя несколько важных уроков из этого описания. 
- исключается утечка объектов, так как все объекты, недоступные от корней приложения, рано или поздно уничтожает уборщик мусора. 
- благодаря уборке мусора невозможно получить доступ к освобожденному объекту с последующим повреждением памяти.

##### Уборка мусора и отладка
Как только объект становится недостижимым, он превращается в кандидата на удаление — объекты далеко не всегда «доживают» до завершения работы метода. Для приложения эта особенность может иметь интересные последствия. 

```cs
public static void Main()
{
    // Создание объекта Timer, вызывающего метод TimerCallback
    // каждые 2000 миллисекунд
    Timer t = new Timer(TimerCallback, null, 0, 2000);

    // Ждем, когда пользователь нажмет Enter
    Console.ReadLine();
}

private static void TimerCallback(Object o)
{
    // Вывод даты/времени вызова этого метода
    Console.WriteLine("In TimerCallback: " + DateTime.Now);
    // Принудительный вызов уборщика мусора в этой программе
    GC.Collect();
}
```

После изучения приведенного кода складывается впечатление, что метод `TimerCallback` будет вызываться каждые 2000 миллисекунд. В конце концов, мы создаем объект `Timer`, на который ссылается переменная `t`. Поскольку таймер существует, он должен срабатывать. Но обратите внимание, что в методе `TimerCallback` процедура уборки мусора вызывается принудительно методом `GC.Collect()`. После запуска уборщик мусора предполагает, что все объекты в куче недостижимы (то есть являются мусором), в том числе объект `Timer`. Затем уборщик проверяет корни приложения и видит, что метод `Main` не использует переменную `t` после присвоения ей значения. Поэтому в приложении нет переменной, ссылающейся на объект `Timer`, и уборщик мусора освобождает занятую им память. В итоге таймер останавливается, а метод `TimerCallback` вызывается всего один раз. 

### Поколения
Уборщик мусора с поддержкой поколений (generational garbage collector), который также называют эфемерным уборщиком мусора (ephemeral garbage collector), работает на основе следующих предположений: 
- чем младше объект, тем короче его время жизни; 
- чем старше объект, тем длиннее его время жизни; 
- уборка мусора в части кучи выполняется быстрее, чем во всей куче.

Сразу после инициализации в управляемой куче нет объектов. Говорят, что создаваемые в куче объекты составляют поколение 0. Проще говоря, к нулевому поколению относятся только что созданные объекты, которых не касался уборщик мусора. Следующий рис. демонстрирует только что запущенное приложение, разместившее в памяти пять объектов (А–Е). Через некоторое время объекты С и Е становятся недоступными.

![gc_2](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_2.png "Рис. 2. Вид.кучи.сразу.после.инициализации:.все.объекты.в.ней.относятся. к.поколению.0,.уборка.мусора.еще.не.выполнялась")

При инициализации CLR выбирает пороговый размер для поколения 0. Если в результате выделения памяти для нового объекта размер поколения 0 превышает пороговое значение, должна начаться уборка мусора. Допустим, объекты А–Е относятся к поколению 0. Тогда при размещении объекта F должна начаться уборка мусора. Уборщик мусора определяет, что объекты С и E — это мусор, и выполняет сжатие памяти для объекта D, перемещая его вплотную к объекту B. Объекты, пережившие уборку мусора (А, В и D), становятся поколением 1. Объекты из поколения 1 были проверены уборщиком мусора один раз. 

![gc_3](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_3.png "Рис. 3. Вид.кучи.после.одной.уборки.мусора:.выжившие.объекты.из.поколения.0. переходят.в.поколение.1,.поколение.0.пустует")

После уборки мусора объектов в поколении 0 не остается. Туда помещаются новые объекты. Как показано на следующем рис, приложение продолжает работу и размещает объекты F–K. Также в ходе работы приложения становятся недоступными объекты B, H и J, поэтому занятая ими память должна рано или поздно освободиться.

![gc_4](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_4.png "Рис. 4. В.поколении.0.появились.новые.объекты,.в.поколении.1.—.мусор")

А теперь представьте, что при попытке размещения объекта L размер поколения 0 превысил пороговое значение, поэтому должна начаться уборка мусора. При этом уборщик мусора решает, какие поколения следует обработать. Я уже упоминал, что при инициализации CLR выбирает пороговый размер поколения 0; CLR также выбирает пороговый размер для поколения 1. Начиная уборку мусора, уборщик определяет, сколько памяти занято поколением 1. Пока поколение 1 занимает намного меньше отведенной памяти, поэтому уборщик проверяет только объекты поколения 0. Еще раз просмотрите предположения, на которых базируется работа уборщика мусора. Первое допущение гласит, что у новых объектов время жизни короче. Поэтому в поколении 0, скорее всего, окажется много мусора, и очистка этого поколения освободит много памяти. А поскольку уборщик игнорирует объекты поколения 1, уборка мусора значительно ускоряется. Ясно, что игнорирование объектов поколения 1 повышает быстродействие уборщика. Однако его производительность растет еще больше благодаря выборочной проверки объектов в управляемой куче. Если корень или объект ссылается на объект из старшего поколения, уборщик игнорирует все внутренние ссылки старшего объекта, сокращая время построения графа доступных объектов. Конечно, возможна ситуация, когда старый объект ссылается на новый. Чтобы не пропустить обновленные поля этих старых объектов, уборщик использует внутренний механизм JIT-компилятора, устанавливающий флаг при изменении ссылочного поля объекта. Он позволяет уборщику выяснить, какие из старых объектов (если они есть) были изменены с момента последней уборки мусора. Остается проверять только старые объекты с измененными полями, чтобы выяснить, не ссылаются ли они на новые объекты из поколения 0.

Уборщик мусора с поддержкой поколений также предполагает, что объекты, прожившие достаточно долго, продолжат жить и дальше. Так что велика вероятность, что объекты поколения 1 и впредь останутся доступными в приложении. То есть проверив объекты поколения 1, уборщик нашел бы мало мусора и не смог бы освободить много памяти. Следовательно, уборка мусора в поколении 1, скорее всего, окажется пустой тратой времени. Если в поколении 1 появляется мусор, он просто остается там. 

![gc_5](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_5.png "Рис. 5. Вид.кучи.после.двух.операций.уборки.мусора:.выжившие.объекты. из.поколения.0.переходят.в.поколение.1.(увеличивая.его.размер),. поколение.0.пустует")

Как видите, все объекты из поколения 0, пережившие уборку мусора, перешли в поколение 1. Так как уборщик не проверяет поколение 1, память, занятая объектом В, не освобождается, даже если этот объект на момент уборки мусора недоступен. И в этот раз после уборки мусора поколение 0 пустеет, в это поколение попадут новые объекты. Допустим, приложение работает дальше и выделяет память под объекты L–O. Во время работы приложение прекращает использовать объекты G, L и M, и они становятся недоступными.

![gc_6](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_6.png "Рис. 6. В.поколении.0.созданы.новые.объекты,.количество.мусора. в.поколении.1.увеличилось")

Допустим, в результате размещения объекта P размер поколения 0 превысил пороговое значение, что инициировало уборку мусора. Поскольку все объекты поколения 1 занимают в совокупности меньше порогового уровня, уборщик вновь решает собрать мусор только в поколении 0, игнорируя недоступные объекты в поколении 1 (B и G). 

![gc_7](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_7.png "Рис. 7. Вид.кучи.после.трех.операций.уборки.мусора:.выжившие.объекты. из.поколения.0.переходят.в.поколение.1.(увеличивая.его.размер);. поколение.0.пустеет")

На рисунке видно, что поколение 1 постепенно растет. Допустим, поколение 1 выросло до таких размеров, что все его объекты в совокупности превысили пороговое значение. В этот момент приложение продолжает работать (потому что уборка мусора только что завершилась) и начинает размещение в памяти объектов P–S, которые заполняют поколение 0 до его порогового значения.

![gc_8](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_8.png "Рис. 8. Новые.объекты.размещены.в.поколении.0,. в.поколении.1.появилось.больше.мусора")

При попытке приложения разместить объект Т поколение 0 заполняется и начинается уборка мусора. Однако на этот раз уборщик мусора обнаруживает, что место, занятое объектами, превысило пороговое значение. После нескольких операций уборки мусора в поколении 0 велика вероятность, что несколько объектов в поколении 1 стали недоступными (как в нашем примере). Поэтому теперь уборщик мусора проверяет все объекты поколений 1 и 0. 

![gc_9](https://gitlab.com/Grigorenko1/clr/raw/master/data/4.KeyMechanisms/4.2.GC_9.png "Рис. 9. Вид.кучи.после.четырех.операций.уборки.мусора:.выжившие.объекты. из.поколения.1.переходят.в.поколение.2,.выжившие.объекты.из.поколения.0. переходят.в.поколение.1,.поколение.0.снова.пусто")

Все выжившие объекты поколения 0 теперь находятся в поколении 1, а все выжившие объекты поколения 1 — в поколении 2. Как всегда, сразу после уборки мусора поколение 0 пустеет: в нем будут размещаться новые объекты. В поколении 2 находятся объекты, проверенные уборщиком мусора не меньше двух раз. Операций уборки мусора может быть много, но объекты поколения 1 проверяются только тогда, когда их суммарный размер достигает порогового значения — до этого обычно проходит несколько операций уборки мусора в поколении 0. Управляемая куча поддерживает только три поколения: 0, 1 и 2. Поколения 3 не существует. При инициализации в CLR устанавливается пороговое значение для всех трех поколений. Уборщик мусора CLR является самонастраивающимся, то есть в процессе работы он анализирует функциональность приложения и адаптируется.

##### Запуск уборки мусора
Как вы уже знаете, CLR запускает уборку мусора, когда обнаруживает, что объем поколения 0 достиг своего порогового значения. Это самая распространенная причина запуска уборки мусора, однако есть и другие: 
- **Вызов статического метода `Collect` объекта `System.GC`.** Код явно указывает, в какой момент должна быть выполнена уборка мусора. Хотя Microsoft решительно не рекомендует использовать этот метод, иногда принудительная уборка мусора в приложении может быть оправдана.
- **Windows сообщает о нехватке памяти.** CLR использует функции Win32 *CreateMemoryResourceNotification* и *QueryMemoryResourceNotification* для контроля состояния памяти системы. Если Windows сообщает о недостаточном объеме свободной памяти, CLR запускает уборку мусора, чтобы избавиться от неиспользуемых объектов и сократить размер рабочего набора процесса. 
- **Выгрузка домена приложения.** При выгрузке домена приложения CLR выполняет полную уборку мусора для всех поколений.
- **Завершение работы CLR.** CLR завершает работу при нормальном завершении процесса. Во время заверения CLR считает, что в процессе нет корневых ссылок; объектам предоставляется возможность выполнить очистку, но CLR не пытается дефрагментировать или освобождать память, потому что после завершения всего процесса Windows автоматически освобождает всю его память.

##### Большие объекты
Существует еще один путь повышения быстродействия, о котором стоит рассказать. CLR делит объекты на малые и большие. До настоящего момента рассматривались только малые объекты. Любые объекты размером 85 000 байт и более считаются большими. CLR работает с большими объектами по несколько отличающимся правилам: 
- Память для них выделяется в отдельной части адресного пространства процесса. 
- К большим объектам не применяется сжатие, так как на их перемещение в памяти потребуется слишком много процессорного времени.
- Большие объекты всегда считаются частью поколения 2, поэтому их следует создавать лишь для ресурсов, которые должны жить долго. Размещение в памяти короткоживущих больших объектов приведет к необходимости частой уборки мусора в поколении 2, что снижает производительность. 

##### Режимы уборки мусора
При запуске CLR выбирается один из режимов уборки мусора, который не может быть изменен до завершения процесса. Существует два основных режима уборки мусора: 
- **Режим рабочей станции.** Этот режим настраивает уборку мусора для приложений на стороне клиента. Он оптимизирован для минимизации времени приостановки потоков приложения, чтобы не раздражать пользователя. Уборщик предполагает, что на компьютере работают другие приложения, и старается не занимать слишком много ресурсов процессора. 
- **Режим сервера.** Этот режим оптимизирует уборку мусора для приложений на стороне сервера. Уборщик предполагает, что на машине не запущено никаких сторонних приложений (клиентских или серверных), поэтому все ресурсы процессора можно бросить на уборку мусора. В этом режиме управляемая куча разбирается на несколько разделов — по одному на процессор. Изначально уборщик мусора использует один поток на один процессор. Каждый поток выполняется в собственном разделе одновременно с другими потоками. Такой подход хорошо работает в случае приложений с единообразным поведением рабочих потоков. Функция работает на компьютерах с несколькими процессорами; только в этом случае параллельная обработка потоков позволяет получить прирост производительности. 

Кроме двух основных режимов, у уборщика мусора существует два подрежима: *параллельный* (используемый по умолчанию) и *непараллельный*.

 В параллельном режиме у уборщика мусора есть дополнительный фоновый поток, выполняющий пометку объектов во время работы приложения. Когда поток размещает в памяти объект, вызывающий превышение порога для поколения 0, уборщик сначала приостанавливает все потоки, а затем определяет поколения, в которых нужно выполнить уборку мусора. Если уборщик должен собрать мусор в поколении 0 или 1, он работает как обычно, но если нужно собрать мусор в поколении 2, размер поколения 0 увеличивается выше порогового, чтобы разместить новый объект, а затем исполнение потоков приложения возобновляется. Пока работают потоки приложения, отдельный поток уборщика с нормальным приоритетом находит все недоступные объекты в фоновом режиме. После того как объекты будут обнаружены, уборщик приостанавливает все потоки и решает, нужно ли дефрагментировать память. Если он принимает положительное решение, память дефрагментируется, ссылки корней исправляются, а исполнение потоков приложения возобновляется — такая уборка мусора обычно проходит быстрее, так как перечень недоступных объектов создается заранее. Однако уборщик может отказаться от дефрагментации памяти, что, на самом деле, предпочтительнее. Если свободной памяти много, уборщик не станет дефрагментировать кучу — это повышает быстродействие, но увеличивает рабочий набор приложения. Прибегая к параллельной уборке мусора, приложение обычно расходует больше памяти, чем при непараллельной уборке. 
 
 
 ##### Программное управление уборщиком мусора
 Тип `System.GC` позволяет приложению напрямую управлять уборщиком мусора.
 Чтобы заставить уборщика мусора провести уборку, следует вызвать метод `Collect` класса `GC`. При вызове можно указать поколение, в котором нужно выполнить уборку мусора, параметр `GCCollectionMode` и логический признак выполнения блокирующей (непараллельной) или фоновой (параллельной) уборки мусора. 

Различные значения параметра `GCCollectionMode`:

| Значение | Описание |
| :------- | -------- |
| Default | Аналогично вызову метода `GC.Collect` без флагов. В настоящее время эквивалентно передаче параметра `Forced`, но в следующих версиях CLR ситуация может измениться |
| Forced | Инициирует уборку мусора для всех поколений вплоть до указанного вами, включая и само это поколение | 
| Optimized | Уборка мусора осуществляется только при условии качественного конечного результата, выражающегося либо в освобождении большого объема памяти, либо в уменьшении фрагментации. В противном случае вызов метода в этом режиме не дает никакого эффекта |

**Обычно следует избегать вызова любых методов `Collect`**: лучше не вмешиваться в работу уборщика мусора и позволить ему самостоятельно настраивать пороговые значения для поколений, основываясь на реальном поведении приложения. 

Например, имеет смысл вызывать метод `Collect`, если только что произошло некое разовое событие, которое привело к уничтожению множества старых объектов. Вызов `Collect` в такой ситуации очень кстати, ведь основанные на прошлом опыте прогнозы уборщика мусора, скорее всего, для разовых событий окажутся неточными. 

##### Мониторинг использования памяти приложением
Существуют методы, которые можно вызвать для наблюдения за работой уборщика мусора в процессе. Так, следующие статические методы класса GC вызываются для выяснения числа операций уборки мусора в конкретном поколении или для объема памяти, занятого в данный момент объектами в управляемой куче.

```cs
Int32 CollectionCount(Int32 generation);
Int64 GetTotalMemory(Boolean forceFullCollection);
```

Чтобы выполнить профилирование конкретного блока кода, я часто вставляю до и после него код, вызывающий эти методы, а затем вычисляю разность. Это позволяет мне судить о том, как этот блок кода сказывается на рабочем наборе процесса, и узнать, сколько операций уборки мусора произошло при исполнении этого блока кода.

##### Освобождение ресурсов при помощи механизма финализации
Большинству типов для работы требуется только память, но есть и типы, которым помимо памяти необходимы системные ресурсы. Например, типу `System.IO.FileStream` нужно открыть файл (системный ресурс) и сохранить его дескриптор. Затем при помощи этого дескриптора методы `Read` и `Write` данного типа работают с файлом.

Если тип, использующий системный ресурс, будет уничтожен в ходе уборки мусора, занимаемая объектом память вернется в управляемую кучу; однако системный ресурс, о котором уборщику мусора ничего не известно, будет потерян. Разумеется, это нежелательно, поэтому CLR поддерживает механизм финализации (finalization), позволяющий объекту выполнить корректную очистку, прежде чем уборщик мусора освободит занятую им память. 

Всеобщий базовый класс System.Object определяет защищенный виртуальный метод с именем `Finalize`. Когда уборщик мусора определяет, что объект подлежит уничтожению, он вызывает метод Finalize этого объекта (если он переопределен). Группа проектировщиков C# из Microsoft посчитала, что метод финализации отличается от остальных и требует специального синтаксиса в языке программирования. Поэтому для определения метода финализации в С# перед именем класса нужно добавить знак тильды (~): 

```cs
~SomeType() { // Код метода финализации }
```

Методы `Finalize` вызываются при завершении уборки мусора для объектов, которые уборщик мусора определил для уничтожения. Это означает, что память таких объектов не может быть освобождена немедленно, потому что метод `Finalize` может выполнить код с обращением к полю. Так как финализируемый объект должен пережить уборку мусора, он переводится в другое поколение, вследствие чего такой объект живет намного дольше, чем следует. Ситуация не идеальна в отношении использования памяти, поэтому финализации следует по возможности избегать. Проблема усугубляется тем, что при преобразовании поколения финализируемых объектов все объекты, на которые они ссылаются в своих полях, тоже преобразуются, потому что они должны продолжать свое существование. Итак, старайтесь по возможности обойтись без создания финализируемых объектов с полями ссылочного типа.

Также следует учитывать, что разработчик не знает, в какой именно момент будет выполнен метод `Finalize`, и не может управлять его выполнением. Методы `Finalize` выполняются при выполнении уборки мусора, которая может произойти тогда, когда ваше приложение запросит дополнительную память. Кроме того, CLR не дает никаких гарантий относительно порядка вызова методов `Finalize`. Итак, следует избегать написания методов `Finalize`, обращающихся к другим объектам, типы которых определяют метод `Finalize`; может оказаться, что последние уже прошли финализацию. 