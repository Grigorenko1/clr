# Clr

[Common Language Runtime](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#common-language-runtime)

&nbsp;&nbsp;&nbsp;&nbsp;[assembly](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#assembly)

&nbsp;&nbsp;&nbsp;&nbsp;[manifest](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#manifest)

&nbsp;&nbsp;&nbsp;&nbsp;[IL](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#il)

&nbsp;&nbsp;&nbsp;&nbsp;[FCL](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#fcl)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Пространства имен](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#пространства-имен)

&nbsp;&nbsp;&nbsp;&nbsp;[CTS](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#cts)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Спецификация](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#спецификация)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Правила видимости типов и доступа к членам типа](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#правила-видимости-типов-и-доступа-к-членам-типа)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Правило CTS](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#правило-cts)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ECMA](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#ecma)

&nbsp;&nbsp;&nbsp;&nbsp;[CLS](https://gitlab.com/Grigorenko1/clr/blob/master/docs/1.Clr/clr.md#cls)

# Types

[System.Object](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#systemobject)

&nbsp;&nbsp;&nbsp;&nbsp;[Открытые методы](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#открытые-методы)

&nbsp;&nbsp;&nbsp;&nbsp;[Защищенные методы](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#защищенные-методы)

[Оператор new](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#оператор-new)

&nbsp;&nbsp;&nbsp;&nbsp;[Действия оператор new](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#действия-оператор-new-)

[Приведение типов](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#приведение-типов)

&nbsp;&nbsp;&nbsp;&nbsp;[Преимущество безопасности типов](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#преимущество-безопасности-типов)

[Оператор is](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#оператор-is)

[Оператор as](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#оператор-as)

[Пространства имен и сборки](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#пространства-имен-и-сборки)

&nbsp;&nbsp;&nbsp;&nbsp;[Другая форма директивы using](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#другая-форма-директивы-using)

&nbsp;&nbsp;&nbsp;&nbsp;[Директива namespace](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#директива-namespace)

[Взаимодействие компонентов во время выполнения](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#взаимодействие-компонентов-во-время-выполнения)

[Пример для исполняемой среды CLR](https://gitlab.com/Grigorenko1/clr/blob/master/docs/2.Types/1.CommonTypes.md#пример-для-исполняемой-среды-clr)

# Primitives

[Примитивные типы]()