﻿
[Статьи](https://gitlab.com/Grigorenko1/clr/-/blob/master/articles)

- [Различные концепции равенства](https://gitlab.com/Grigorenko1/clr/-/blob/master/articles/4-way-equals-objects.md)
- [MVC. Маршрутизация.](https://gitlab.com/Grigorenko1/clr/-/blob/master/articles/mvc-routing.md)

- [Парсинг строк в числа, используя перечисление NumberStyles](https://gitlab.com/Grigorenko1/clr/-/blob/master/articles/parsing-number.md)
- [Парсинг даты](https://gitlab.com/Grigorenko1/clr/-/blob/master/articles/parsing-date.md)

- [SOLID](https://gitlab.com/Grigorenko1/clr/-/blob/master/articles/solid.md)


External

- \[habr\] [Юнит-тестирование для чайников](https://habr.com/ru/post/169381/)
- \[habr\] [14 вопросов об индексах в SQL Server, которые вы стеснялись задать](https://habr.com/ru/post/247373/)
- \[habr\] [Уровни изолированности транзакций для самых маленьких](https://habr.com/ru/post/469415/)

- [Тесты](http://www.quizful.net/test)

- \[tproger\] [Программа минимум: что должен знать начинающий C# программист](https://tproger.ru/translations/csharp-basic-skills/)
- \[tproger\] [Алгоритмы и структуры данных для начинающих: сложность алгоритмов](https://tproger.ru/translations/algorithms-and-data-structures/)

Patterns

- \[tproger\] [Шаблоны проектирования простым языком.](https://tproger.ru/translations/design-patterns-simple-words-1/?utm_source=grf-eng&utm_medium=partner&utm_campaign=giraff.io)
- [Design Patterns in C# - Introducing the Daily Design Pattern](https://www.exceptionnotfound.net/introducing-the-daily-design-pattern/)
- \[metanit\] [Паттерны проектирования в C# и .NET](https://metanit.com/sharp/patterns/)
- [.NET Design Patterns](https://www.dofactory.com/net/design-patterns)
- \[tproget\] [Шаблоны проектирования для новичков](https://tproger.ru/translations/design-patterns-for-beginners/)
- [DESIGN PATTERNS](https://refactoring.guru/design-patterns)

Tests

- [How to Successfully Mock ASP.NET HttpContext](https://www.danylkoweb.com/Blog/how-to-successfully-mock-httpcontext-BT)
- [Fluent Assertions](https://fluentassertions.com/)
- [xUnit](https://xunit.net/)

SOLID

- \[proglib\] [Протестируй это: принципы и законы создания тестируемого кода](https://proglib.io/p/testable-code/)
- \[medium\] [Understanding SOLID Principles: Liskov Substitution Principle](https://codeburst.io/understanding-solid-principles-liskov-substitution-principle-e7f35277d8d5)
- [SOLID Design Principles Explained: The Single Responsibility Principle](https://stackify.com/solid-design-principles/)
- \[medium\] [SOLID Principles made easy](https://medium.com/@dhkelmendi/solid-principles-made-easy-67b1246bcdf)
- [S.O.L.I.D: The First 5 Principles of Object Oriented Design](https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)

DDD

- [Best Practices for Event-Driven Microservice Architecture](https://hackernoon.com/best-practices-for-event-driven-microservice-architecture-e034p21lk)
- \[fowler\] [Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html)
- [The Domain - First Pop Coffee Company](https://buildplease.com/pages/fpc-1/)
	- [Publishing Events to the Bus](https://buildplease.com/pages/fpc-19/)
- \[docs\] [Design a DDD-oriented microservice](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/ddd-oriented-microservice)

- [Using MediatR in ASPNET Core Apps](https://ardalis.com/using-mediatr-in-aspnet-core-apps)
- \[github\] [MediatR](https://github.com/jbogard/MediatR/wiki/Behaviors)

- [NATS](https://docs.nats.io/)
- [Pulsar](https://pulsar.apache.org/docs/en/standalone/)

OOP
- \[tproger\] [Введение в ООП с примерами на C#. Часть первая. Все, что нужно знать о полиморфизме](https://tproger.ru/translations/diving-in-oop-p1/)
- \[tproger\] [Введение в ООП с примерами на C#. Часть вторая. Все, что нужно знать о наследовании](https://tproger.ru/translations/diving-in-oop-p2/)
- \[tproger\] [Введение в ООП с примерами на C#. Часть третья. Практические аспекты использования полиморфизма](https://tproger.ru/translations/diving-in-oop-p3/)
- \[tproger\] [Введение в ООП с примерами на C#. Часть четвёртая. Абстрактные классы](https://tproger.ru/translations/diving-in-oop-p4/)
- \[tproger\] [Введение в ООП с примерами на C#. Часть пятая. Всё о модификаторах доступа](https://tproger.ru/translations/diving-in-oop-p5/)


Microservices

- \[habr\] [Микросервисы на платформе .NET](https://habr.com/company/piter/blog/347846/)

Logging

- [Ultimate NLog Tutorial for .NET Logging](https://medium.stackify.com/ultimate-nlog-tutorial-for-net-logging-19-best-practices-resources-and-tips-1d5b7753afd2)
- [What Is Structured Logging and Why Developers Need It](https://stackify.com/what-is-structured-logging-and-why-developers-need-it/)
- \[habr\] [Правильное логгирование в Microsoft .NET Framework](https://habr.com/ru/post/98638/)
- \[docs\] [Logging in .NET Core and ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging/?tabs=aspnetcore2x&view=aspnetcore-2.2#how-to-add-providers)


Regex

- [debuggex.com](https://www.debuggex.com/)
- \[medium\] [Regex tutorial — A quick cheatsheet by examples](https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285)
- [Регулярные выражения в C#](https://professorweb.ru/my/csharp/charp_theory/level4/4_10.php)

C#

- \[docs\] [What's new in C# 8.0](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8)
- [Using C# 8 and Nullable Reference Types in .NET Framework](https://www.infoq.com/articles/CSharp-8-Framework/)
- \[habr\] [События C# по-человечески](https://habr.com/ru/post/213809/)
- \[stackovf\] [Why would you use Expression<Func<T>> rather than Func<T>?](https://stackoverflow.com/questions/793571/why-would-you-use-expressionfunct-rather-than-funct)
- [Lambda expressions](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/statements-expressions-operators/lambda-expressions)

HashSet
HashTable
ReadonlyCollection
valuetask c#
span

- [Autofac](https://github.com/autofac/Autofac/releases/tag/v5.0.0)

- \[habr\] [Некоторые аспекты оптимизации LINQ-запросов в C#.NET для MS SQL Server](https://habr.com/ru/post/459716/)
- \[habr\] [Методы оптимизации LINQ-запросов в C#.NET](https://habr.com/ru/post/489226/)
- \[habr\] [Async programming in .NET: Best practices](https://habr.com/ru/company/jugru/blog/491236/)
- [Your C# is already functional, but only if you let it](https://hmemcpy.com/2020/03/your-csharp-is-already-functional/)
- [Youtube Video: Difference between Throw and Throw Exception in C#](https://dev.to/jalpeshvadgama/youtube-video-difference-between-throw-and-throw-exception-in-c-57a9)
- [C#: Different ways to Check for Null](https://www.thomasclaudiushuber.com/2020/03/12/c-different-ways-to-check-for-null/)

 C# 8.0

- [Learn C# 8.0](https://www.c-sharpcorner.com/learn/learn-c-sharp-80)
- [C# 8 asynchronous streams](https://developers.redhat.com/blog/2020/02/24/c-8-asynchronous-streams/)


ASP.NET

- \[docs\] [Tutorial: Create a Razor Pages web app with ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/tutorials/index?view=aspnetcore-2.1#how-to-download-a-sample)

- \[docs\] [Add validation to an ASP.NET Core MVC app](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/validation?view=aspnetcore-2.2)
- \[docs\] [Model validation in ASP.NET Core MVC and Razor Pages](https://docs.microsoft.com/en-us/aspnet/core/mvc/models/validation?view=aspnetcore-2.2)
- [fluentvalidation](https://fluentvalidation.net)

- [ASP.NET Core 2.0 Authentication and Authorization System Demystified](https://digitalmccullough.com/posts/aspnetcore-auth-system-demystified.html)
- \[docs\] [Two-factor authentication with SMS in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/security/authentication/2fa?view=aspnetcore-1.1&viewFallbackFrom=aspnetcore-2.1)

- \[docs\] [Safe storage of app secrets in development in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-2.1&tabs=windows#how-the-secret-manager-tool-works)

- \[docs\] [CORS](https://docs.microsoft.com/en-us/aspnet/web-api/overview/security/enabling-cross-origin-requests-in-web-api#how-cors-works)

- \[metanit\] [Атрибут ResponseCache](https://metanit.com/sharp/aspnet5/14.2.php)
- \[docs\] [Response Caching Middleware in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/performance/caching/middleware?view=aspnetcore-2.2)
- \[docs\] [Сжатие ответов в ASP.NET Core](https://docs.microsoft.com/ru-ru/aspnet/core/performance/response-compression?view=aspnetcore-2.2)
- \[docs\] [Image Tag Helper in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/tag-helpers/built-in/image-tag-helper?view=aspnetcore-3.0)

- [ASP.NET Core 2.1: Улучшения WebAPI](https://andrey.moveax.ru/post/asp-net-core-2-1-webapi-improvements)
- \[docs\] [ASP.NET Core Performance Best Practices](https://docs.microsoft.com/en-us/aspnet/core/performance/performance-best-practices?view=aspnetcore-2.2)
- \[docs\] [Analyze .NET Framework memory issues](https://docs.microsoft.com/en-us/visualstudio/misc/analyze-dotnet-framework-memory-issues?view=vs-2015#Anchor_4)

- \[github\] [samples for ASP.NET Core](https://github.com/dodyg/practical-aspnetcore/)
- [10 Performance-Improvement Tips for ASP.NET Core 3.0 Applications](https://www.syncfusion.com/blogs/post/10-performance-improvement-tips-for-asp-net-core-3-0-applications.aspx/amp)
- \[proglib\] [Как написать код, который полюбят все](https://proglib.io/p/kak-napisat-kod-kotoryy-polyubyat-vse-2020-03-17)

MS Build

- \[docs\] [Common MSBuild project items](https://docs.microsoft.com/en-us/visualstudio/msbuild/common-msbuild-project-items?view=vs-2019)
- \[docs\] [A mapping between project.json and csproj properties](https://docs.microsoft.com/en-us/dotnet/core/tools/project-json-to-csproj)
- \[docs\] [Преобразование web.config](https://docs.microsoft.com/ru-ru/aspnet/core/host-and-deploy/iis/transform-webconfig?view=aspnetcore-2.2)
- \[docs\] [Использование нескольких сред в ASP.NET Core](https://docs.microsoft.com/ru-ru/aspnet/core/fundamentals/environments?view=aspnetcore-2.2)
- \[docs\] [Профили публикации Visual Studio (.pubxml) для развертывания приложений ASP.NET Core](https://docs.microsoft.com/ru-ru/aspnet/core/host-and-deploy/visual-studio-publish-profiles?view=aspnetcore-2.2)

SQL

- \[docs\] [MERGE](https://docs.microsoft.com/en-us/sql/t-sql/statements/merge-transact-sql?view=sql-server-ver15)
- \[docs\] [JSON data in SQL Server](https://docs.microsoft.com/en-us/sql/relational-databases/json/json-data-sql-server?view=sql-server-ver15)
- \[docs\] [RAISERROR](https://docs.microsoft.com/en-us/sql/t-sql/language-elements/raiserror-transact-sql?view=sql-server-ver15)
- [SQL Server Tutorial](https://www.sqlservertutorial.net/)
- [SQL Server Tips, Articles and Training](https://www.mssqltips.com/)

WITH (NOLOCK)

Algorithms

- \[github\] [Classic algorithms on C#](https://github.com/shkolovy/classic-algorithms-c-sharp)
- \[github\] [C# ALGORITHMS](https://github.com/aalhour/C-Sharp-Algorithms)
- \[proglib\] [Сортировки в гифках: 8 самых популярных алгоритмов](https://proglib.io/p/sort-gif/)
- \[proglib\] [10 структур данных, которые вы должны знать (+видео и задания)](https://proglib.io/p/data-structures/)
- \[proglib\] [10 алгоритмов на графах в гифках](https://proglib.io/p/graphs-algoguide/)
- [Longest common subsequence problem](https://en.wikipedia.org/wiki/Longest_common_subsequence_problem)

JMeter

- [jmeter](https://jmeter.apache.org/usermanual/build-test-plan.html)

- [youtube](https://www.youtube.com/playlist?list=PL55b2m_9SYspMgqtwdBcUR15wGb5Mq-Gy)
- [youtube](https://www.youtube.com/channel/UCnKJ63mrfDFOOG_n2hLu7PQ/playlists)
- [youtube](https://www.youtube.com/channel/UCTt7pyY-o0eltq14glaG5dg/playlists)
- [youtube](https://www.youtube.com/channel/UCsdoSHH5bucBf_wwtvWJfnQ/playlists)
- [youtube](https://www.youtube.com/channel/UCVOCoXYsZtLyr56Z3NZT2hQ/playlists)

- [6 Tips for JMeter If Controller Usage](https://www.blazemeter.com/blog/six-tips-for-jmeter-if-controller-usage)
- [How can we pass random values from the selected values in jMeter?](https://stackoverflow.com/questions/27144039/how-can-we-pass-random-values-from-the-selected-values-in-jmeter)

Event sourcing

- \[github\] [EventSourcing.NetCore](https://github.com/oskardudycz/EventSourcing.NetCore)
- [EventBus](https://aspnetboilerplate.com/Pages/Documents/EventBus-Domain-Events)
- \[docs\] [Event Sourcing pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/event-sourcing)
- \[docs\] [Command and Query Responsibility Segregation (CQRS) pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/cqrs)
- [A simple Todo application, a comparison on Traditional vs CQRS + ES architecture](https://www.bouvet.no/bouvet-deler/utbrudd/a-simple-todo-application-a-comparison-on-traditional-vs-cqrs-es-architecture)
- [CQRS](http://cqrs.nu/)


Other

- \[habr\] [Децентрализованные приложения. Технология Blockchain в действии](https://habr.com/company/piter/blog/321014/)
- \[medium\] [Blockchain explained using C# implementation](https://towardsdatascience.com/blockchain-explained-using-c-implementation-fb60f29b9f07)
- [Functional Programming in C](https://hownot2code.com/2016/12/14/functional-programming-in-csharp/)


- \[youtube\] [Краткая визуализация книг](https://www.youtube.com/watch?v=65Xci_uI_e0)

- \[docs\] [Books](https://blogs.msdn.microsoft.com/mssmallbiz/2017/07/11/largest-free-microsoft-ebook-giveaway-im-giving-away-millions-of-free-microsoft-ebooks-again-including-windows-10-office-365-office-2016-power-bi-azure-windows-8-1-office-2013-sharepo/)
- \[tproger\] [Стань мастером C#: подборка книг по языку родом из Microsoft](https://tproger.ru/books/csharp-books/)
- [books](https://www.twirpx.com/file/1629188)
- \[github\] [Clean Code concepts adapted for .NET/.NET Core](https://github.com/thangchung/clean-code-dotnet)

- \[youtube\] [hash tables and hash functions](https://www.youtube.com/watch?v=KyUTuwz_b7Q)
- \[docs\] [.NET Microservices Architecture Guidance](https://dotnet.microsoft.com/learn/web/microservices-architecture)
- \[docs\] [Background tasks with hosted services in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-3.0&tabs=visual-studio)
- \[github\] [.NET Core PerformanceMonitor](https://github.com/dotnet-architecture/PerformanceMonitor)
- \[github\] [.NET Application Architecture](https://github.com/dotnet-architecture)
- \[habr\] [Оптимизация производительности .NET (C#) приложений](https://habr.com/ru/post/466931/)
- [ASP.net Core 2.0 MVC & Razor Pages for Beginners How to Build a Website](https://www.scribd.com/document/390878739/ASP-net-Core-2-0-MVC-Razor-Pages-for-Beginners-How-to-Build-a-Website)
- [Repository Pattern](https://deviq.com/repository-pattern/)
- [Specification Pattern](https://deviq.com/specification-pattern/)
- \[github\] [Microsoft eShopOnWeb ASP.NET Core Reference Application](https://github.com/dotnet-architecture/eShopOnWeb)
- \[github\] [Computer-Security-algorithms](https://github.com/zeyadetman/Computer-Security-algorithms)
- [ravendb](https://ravendb.net/learn)
- [Telegram Bot with Calendar Picker](https://github.com/karb0f0s/CalendarPicker)

- [restful api testing](https://www.onlinefreecourse.net/restful-api-testing-with-postman-udemy-free-download)
- [Top Tutorials To Learn POSTMAN For REST API Testing](https://medium.com/quick-code/top-tutorials-to-learn-postman-for-rest-api-testing-3bdf9788e0ba)

- [Browse code samples](https://docs.microsoft.com/en-us/samples/browse/?redirectedfrom=MSDN-samples)

- [Memory Leak C#](https://codewithshadman.com/memory-leak-c/)
- \[habr\] [Как я свой мессенджер писал](https://habr.com/ru/post/490070/)
- [Разбираемся с применением паттернов в LINQ, foreach и асинхронных методах](https://www.gamlor.info/posts-output/2019-12-11-csharp-structural-pattern/en/)

VS

- [10 Visual Studio Tips & Tricks You Probably DON'T KNOW](https://devdigest.today/post/1136)
- [10 Essential Debugging Tools for C# .NET Development](https://michaelscodingspot.com/dotnet-debugging-tools/)

Interview

- [Формула успешного собеседования: эго, сторителлинг и выстрел в десятку](https://blog.rabota.ua/jobinterview/)

- [Не просто слова: 10 секретов убедительного сопроводительного письма](https://blog.rabota.ua/coverletter/)

- [Все, что нужно знать о вопросе зарплаты при поиске работы: ликбез от рекрутера](https://blog.rabota.ua/vse-chto-nuzhno-znat-o-voprose-zarplaty-pry-poyske-raboty-lykbez-ot-rekrutera/)

- [Как оценивают тестовой задание](https://dou.ua/lenta/articles/test-task/?from=nl&utm_source=20181023&utm_medium=email&utm_campaign=CM)

- [Вопросы, которые программистам на самом деле стоит задавать на собеседованиях](https://tproger.ru/translations/job-interview-questions-guide-for-programmers/)

- [Как подготовиться к собеседованию, чтобы получить желаемую должность: советы от backend-разработчикa](https://tproger.ru/articles/interview-prep-advice/)


News

- [techcrunch](https://techcrunch.com/)
- [feedly](https://feedly.com/i/discover/sources/search/topic/programming)
- [channel9](https://channel9.msdn.com/)

Developers

##### Vladimir Khorikov
- [pluralsight](https://www.pluralsight.com/courses/csharp-applying-functional-principles)
- [github](https://github.com/vkhorikov)
- [blog](http://enterprisecraftsmanship.com/)

- [blog](https://morioh.com/topic/c-sharp)

- [blog](https://blog.adamfurmanek.pl/2016/05/21/virtual-and-non-virtual-calls-in-c/)

- [blog](https://wakeupandcode.com/aspnetcore/)

- [blog](https://andrewlock.net/)

3DS MAX
- [youtube](https://www.youtube.com/watch?v=Au7MqJs1MgU&list=PLUKuyC8EacloSJP-s7nfdWMKTfWN-7PQo&index=2)
- [youtube](https://www.youtube.com/channel/UCB3n8naJNS6SL4Vv_87bcog/videos)
- [youtube](https://www.youtube.com/watch?v=y0Qm3pXZ-Qg&list=PLRurWHBo0l2OfdEij1kASuc00Fc50Kt76&index=36)

DESIGN
- [design](https://oselya.ua/project/dom-na-osokorkah-240-kvm-avgust-2019)
- [design](https://oselya.ua/project/skandinavskiy-stil-s-elementami-lofta)

MOVIE
	narko
	http://seasonvar.ru/serial-20758-CHernoe_zerkalo-5-season.html
	[С чистого листа](https://www.youtube.com/channel/UC6AuZhW39jTXgi47wgjEl0g/videos)

ENGLISH
- [Victoria italki](https://www.italki.com/teacher/1723029)

COMMON

- [travel](https://escapewithpro.com/uk/tours/7iad23)
- [crm preview example](https://www.mil-soft.com/)
- \[habr\] [О чем молчат Лиды](https://habr.com/en/post/424887/)
- [бизнес-план](http://www.williamspublishing.com/PDF/978-5-8459-1459-0/part.pdf)
- [shop with some products](https://fre-template.webflow.io/product/corey-moranis-wrap-ring)
- [карта вестероса](https://7kingdoms.ru/map/map_ru.html#3/-422.81/154.00)
- \[youtube\] [offer](https://www.youtube.com/watch?v=VVwWphH430E)
- \[youtube\] [artefact](https://www.youtube.com/watch?v=TKFb0qVJhMM)

Raspberry PI

- [The Raspberry Pi Board Guide: Zero vs. Model A and B](https://www.makeuseof.com/tag/raspberry-pi-board-guide/)
- [26 Awesome Uses for a Raspberry Pi](https://www.makeuseof.com/tag/different-uses-raspberry-pi/)
- [How to Build an Effective and Affordable Smart Home From the Ground Up](https://www.makeuseof.com/tag/build-effective-affordable-smart-home/)
- [Raspberry Pi: The Unofficial Tutorial](https://www.makeuseof.com/tag/great-things-small-package-your-unofficial-raspberry-pi-manual/)
- [Your Guide To Plex – The Awesome Media Center](https://www.makeuseof.com/tag/plex-a-manual-your-media-with-style/)
- [How to Build a Great Media Center PC](https://www.makeuseof.com/tag/how-to-build-a-media-center-home/)
- [How to Customize Windows 10: The Complete Guide](https://www.makeuseof.com/tag/complete-guide-windows-customization/)
- [How to Use Kodi: The Complete Setup Guide](https://www.makeuseof.com/tag/set-up-use-kodi-beginners/)
- [23 Operating Systems That Run on Your Raspberry Pi](https://www.makeuseof.com/tag/7-operating-systems-you-can-run-with-raspberry-pi/)
- [OpenHAB Beginner’s Guide Part 2: ZWave, MQTT, Rules and Charting](https://www.makeuseof.com/tag/openhab-beginners-guide-part-2-zwave-mqtt-rules-charting/)
- [Getting Started with OpenHAB Home Automation on Raspberry Pi](https://www.makeuseof.com/tag/getting-started-openhab-home-automation-raspberry-pi/)
- [Getting Started with Raspberry Pi Zero](https://www.makeuseof.com/tag/getting-started-raspberry-pi-zero/)
- [Ultimate Beginner’s Guide to 3D Printing](https://www.makeuseof.com/tag/beginners-guide-3d-printing/)

- [Learn how to build .NET Core IoT apps for Raspberry Pi Linux, and connect to Azure IoT Hub](https://dev.to/azure/net-core-iot-raspberry-pi-linux-and-azure-iot-hub-learn-how-to-build-deploy-and-debug-d1f)



[Snippets](https://docs.microsoft.com/en-us/visualstudio/ide/code-snippet-functions?view=vs-2019)
EmptyList = Enumerable.Empty<T>();
vat = var t
di = create private readonly variable and map into ctor



http://msbuildbook.com/


QA api
https://www.onlinefreecourse.net/restful-api-testing-with-postman-udemy-free-download/
https://medium.com/quick-code/top-tutorials-to-learn-postman-for-rest-api-testing-3bdf9788e0ba




Func programming
System.Diagnostics.Contracts
 
Contract.Invariant()
Contract.Requires()
 
неизменяемые коллекции (immutable collections)





Restaurant App

- [Restaurant](https://rmagazine.com/why-mobile-payment-systems-are-increasing-tips/)
- [The 3 Best Restaurant Apps Your Startup Needs Now](https://rmagazine.com/articles/the-3-best-restaurant-apps-your-startup-needs-now.html)
- [tabbedout](https://www.tabbedout.com/#_blank)
- [tock](https://www.exploretock.com/join)
- [Tock - Restaurant Reservations](https://apps.apple.com/us/app/tock-restaurant-reservations/id1369347408)
- [Tock Dashboard](https://apps.apple.com/us/app/tock-dashboard/id1272762298)
- [Restaurant Tech Trends: Streamlined Payment Apps, On-demand Explodes + More](https://foodtechconnect.com/2015/06/09/restaurant-tech-trends-streamlined-payment-apps-on-demand-food-delivery-explodes/)

- [ИТ в ресторанном бизнесе: онлайн-заказы, бронирование столиков и автоматизация процессов](https://habr.com/ru/company/jowi/blog/366649/)
- [Автоматизация ресторана: 24 интересных инструмента](https://habr.com/ru/post/258679/)
- [Автоматизация бизнеса: 5 интересных инструментов (и их аналоги)](https://habr.com/ru/post/265049/)
- [Как рестораны создают меню: 4 дизайн-техники](https://habr.com/ru/company/jowi/blog/366059/)
- [Зачем рестораны «гуглят» посетителей](https://habr.com/ru/company/jowi/blog/382537/)
- [Когда красота не нужна: дизайн системы автоматизации ресторана](https://habr.com/ru/company/jowi/blog/382569/)
- [Карандаш vs приложение: Сложности при создании системы автоматизации ресторана](https://habr.com/ru/company/jowi/blog/382273/)
- [Мобильные приложения ресторанов: Чего хотят клиенты](https://habr.com/ru/company/jowi/blog/368731/)
- [Слабое звено: Как хакеры атакуют граждан и организации даже в ресторанах](https://habr.com/ru/company/jowi/blog/366661/)
- [ИТ в ресторанном бизнесе: онлайн-заказы, бронирование столиков и автоматизация процессов](https://habr.com/ru/company/jowi/blog/366649/)
- [Игра в имитацию: Секрет васаби из японских ресторанов](https://habr.com/ru/company/jowi/blog/385055/)
- [Как рестораны создают сайты: 4 дизайн-решения](https://habr.com/ru/company/jowi/blog/384257/)
- [Автоматизация, новые модели оплаты и полезная еда: 7 ресторанных трендов на примере США](https://habr.com/ru/company/jowi/blog/383913/)
- [За и против: Зачем ресторанам сервисы бронирования столиков](https://habr.com/ru/company/jowi/blog/369115/)
- [Мнение инвестора: Почему провалится ваше ресторанное мобильное приложение (и как этого избежать)](https://habr.com/ru/company/jowi/blog/387181/)
- [Как бывший инженер Google разрабатывает новые технологии поиска ресторанов](https://habr.com/ru/company/jowi/blog/387207/)
- [Психология технологий: Как платежные приложения позволяют ресторанам увеличить размер чаевых](https://habr.com/ru/company/jowi/blog/386515/)
- [Какими бывают меню в ресторанах: Электроника, магниты и мел](https://habr.com/ru/company/jowi/blog/386425/)
- [Год 2040: Как могут выглядеть рестораны будущего](https://habr.com/ru/company/jowi/blog/386161/)
- [Назад в будущее: 6 инновационных технологий, меняющих ресторанную индустрию](https://habr.com/ru/company/jowi/blog/388133/)
- [Что география точек Starbucks может сказать об экономической и социальной ситуации в обществе](https://habr.com/ru/company/jowi/blog/388023/)
- [Будущее здесь: Как работает полностью автоматизированный ресторан](https://habr.com/ru/company/jowi/blog/387943/)

applications: 
flypay
Resy
table8
Reserve
Cover app
levelup app